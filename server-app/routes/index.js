const router = require('koa-router')()
const data = require('../json/db.json')
router.get('/', async (ctx, next) => {
  await ctx.render('index', {
    title: 'Hello Koa 2!',
  })
})

router.get('/api/demo-json', async (ctx, next) => {
  const { page = 1, size = 5 } = ctx.query
  ctx.body = {
    code: 0,
    msg: 'success',
    list: data.data.slice((page - 1) * size, page * size),
  }
})

router.get('/string', async (ctx, next) => {
  ctx.body = 'koa2 string'
})

router.get('/json', async (ctx, next) => {
  ctx.body = {
    title: 'koa2 json',
  }
})

module.exports = router
