# day1

#### 小程序介绍

小程序是一种新的开放能力，开发者可以快速地开发一个小程序。小程序可以在微信内被便捷地获取和传播，同时具有出色的使用体验。

基础好还是不好，小程序打开了一扇前端新的大门。
vue/react/ 小程序（22 天学习时间）

#### 小程序开发准备

1. 具备什么样子的能力？

js(可以懂也可以不懂，vue/react)

2. 开发工具

IDE 编辑器

#### 开发流程

1. 注册 （暂时选择-教育类小程序）

- 新的邮箱号码
- 手机号
- 身份证号码
- 微信号码

在微信公众平台注册小程序，完成注册后可以同步进行信息完善和开发。

2. 小程序信息完善

填写小程序基本信息，包括名称、头像、介绍及服务范围等。

3. 开发小程序

完成小程序开发者绑定、开发信息配置后，开发者可下载开发者工具、参考开发文档进行小程序的开发和调试。

4. 提交审核和发布

完成小程序开发后，

#### 小程序目录结构介绍

(npm i treer -g )
app.json 全局配置文件

├─wx-demo-day1
| ├─.eslintrc.js 全局格式化文件
| ├─app.js 全局入口文件（逻辑）
| ├─app.json 全局配置文件（配置路由，公共区域样式，组件，tabBar）
| ├─app.wxss 全局 css 文件
| ├─project.config.json
| ├─project.private.config.json
| ├─sitemap.json
| ├─utils
| | └util.js
| ├─pages
| | ├─logs
| | | ├─logs.js
| | | ├─logs.json
| | | ├─logs.wxml
| | | └logs.wxss
| | ├─index
| | | ├─index.js 逻辑
| | | ├─index.json 配置
| | | ├─index.wxml 排版代码结构
| | | └index.wxss 样式

#### 小程序开发语法

1. wxml 语法： 小程序没有 dom / bom

不能操作 dom

<view></view> 。类似于 <div></div>
<block></block> 类似于 vue 的 <template></template> 是一个虚拟标签
<text></text> 类似于 <span></span>

其他在小程序中看到的标签都是组件形式
eg： <swiper> <swiper-item></swiper-item> </swiper>

2. json 语法

3. js 语法

4. wxss 类似于 css 语法

```css
.box {
  width: 100rpx;
}
```

## 小程序 tabBar 开发

`https://blog.csdn.net/chengqiuming/article/details/126766191`

## day1 总结

1. 小程序介绍-开发准备及工具安装
2. 小程序语法介绍
3. 小程序 tabBar 开发

TabBar: <img src="./tabBar.png">

```js
tabBar 的 6 个组成部分
backgroundColor：tabBar 的背景色
selectedIconPath：选中时的图片路径
borderStyle：tabBar 上边框的颜色
iconPath：未选中时的图片路径
selectedColor：tab 上的文字选中时的颜色
color：tab 上文字的默认（未选中）颜色
```
