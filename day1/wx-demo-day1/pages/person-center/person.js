// pages/person-center/person.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    title: '个人中心标题',
    isOk: true,
    arr: ['健康档案','个人中心', '药品商城', '订单详情', '支付结算']
  },
  
  // 事件绑定
  onClickButton(event) {
      

    // 点击的时候，更新title标题数据
    console.log('event:', event)
    // setData 是一个更新数据的方法，类似于react的setState，但是，
    this.setData({
      title: '更新后的标题',
      isOk: !this.data.isOk
    }, () => {
      console.log('回调中打印', this.data)
    })
    // setData更新后可以立马获取到同步打印数据，不需要采用回调模式
    console.log('打印更新后的数据:', this.data)
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})