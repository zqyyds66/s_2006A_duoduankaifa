// pages/movie/index.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    movieList: [],
    opt: {
      page: 1,
      size: 5
    }
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    this.getMovieList({})  
  },

  getMovieList({page=1, size=5}) {
    wx.request({
      url: 'http://localhost:3000/api/demo-json',
      method: 'GET',
      data: {
        page, 
        size
      },
      success: (res) => {
        console.log('res:', res.data.list)

        this.setData({movieList: [...this.data.movieList, ...res.data.list]})
      },
      fail: (error) => {
        console.log('error:', error)
      }
    })
  },

  onPageClick() {
    this.data.opt.page++
    this.getMovieList({page: this.data.opt.page})
    console.log('this.data.opt.page:', this.data.opt.page)
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {
    this.data.opt.page++
    this.getMovieList({page: this.data.opt.page})
    console.log('this.data.opt.page:', this.data.opt.page)
    console.log('onReachBottom:')
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})