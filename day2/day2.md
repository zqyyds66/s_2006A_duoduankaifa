# day2

```js
// react代码
// 回调函数 定义：  1. 函数 2. 参数 3. setState调用
this.setState(
  {
    count: count + 1,
  },
  () => {}
)
// 回调  -> 高阶函数 -> 高阶组件
arr.map((item) => {})
arr.forEach(() => {})
arr.reduce(() => {}, 0)
arr.find(() => {})
arr.findIndex(() => {})
arr.filter(() => {})
arr.some(() => {})
arr.every(() => {})
arr.sort(() => {})
```

```js
// dom bom EC

arr?.map()
// 线程
// js 单 异步 渲染线程 / 脚本线程
// 小程序 双 渲染线程 / 脚本线程
// 小程序没有dom操作 bom操作

this.setData({}, () => {})
```

# 小程序回顾

1. 介绍
2. 语法

json/js/wxml/wxss

3. 事件绑定

bindTap => onClick

wx:for="{{}}"
wx:key=""
wx:if="{{}}" / wx:else

# 小程序 生命周期

- 全局生命周期：app.js 中的生命周期 onLaunch

1. 登陆
2. 初始化一些全局数据

页面级别生命周期：

1. onLoad 处理 http 请求，
2. onReady
3. onShow 更新数据状态
4. onHide
5. onUnload 销毁定时器

   vue 缓存
   <keep-alive></keep-alive>
   actived
   deactived

-效果生命周期

1. 上拉加载 onReachBottom 需要配合 距离设置
2. 下拉刷新 onPullDownRefresh 需要配合开启配置
3. 分享 onShareAppMessage

# 小程序数据接入第三方数据

1. api 使用
   wx.request({
   url: '',
   method: "",
   data: {},
   success: () => {},
   fail: () => {}
   })

2. koa 后端框架

koa2

npm install -g koa-generator

koa2 <name>

npm i nrm -g 切源工具

nrm use <name>

# 小程序 tabBar-自定义 tabBar-

# 小程序 自定义 导航条 navigation-bar

`配合下拉加载`
